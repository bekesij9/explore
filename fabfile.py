# -*- coding: utf8 -*-
#
# Use this to deploy this on all different servers for testing
#
# First, we use this with virdam and the assets from our test runs
#
# _#fi75D$iL[V
#
# https://194046310792.signin.aws.amazon.com/console
#
from __future__ import absolute_import, print_function
import datetime
import os
import logging


from fabric.api import local, run, env, cd, lcd, put, get, settings, warn_only
from fabric.colors import red, green, cyan, magenta
logger = logging.getLogger(__name__)
logging.basicConfig(filename="fabfile.log",
                    level=logging.INFO,
                    format='%(levelname)s [%(asctime)s]: %(message)s',
                    datefmt='%Y-%m-%d %H:%M')
env.hosts = ['root@virdam.univie.ac.at', "jb@rex001.phaidra.org"]

WORKDIR = {
            'virdam': '/root/work/go/src/ghe.phaidra.org/bekesij9/explore', 
            'rex001': '/home/jb/go/src/ghe.phaidra.org/bekesij9/explore'
        }
PARENT = '/root/work/go'
DESTDIR = 'dest'
LOCAL = os.path.dirname(os.path.realpath(__file__))
NOW = datetime.datetime.now()
NOW_FMT = NOW.strftime("%Y%m%d-%H%M")


def _cur_host():
    "return current hostname"
    return env.host_string.split('@')[-1].split('.')[0]


def go_run(prog='exp-02.go'):
    "run a go program (`go_run:programname`)"
    with cd(WORKDIR[_cur_host()]):
        run("go run {}".format(prog))


def gitupd():
    "update git on virdam"
    with cd(WORKDIR[_cur_host()]):
        local("git push")
        run("git pull")


def go_get(source=None):
    "load a package"
    if not source:
        print("no source!")
    with cd(WORKDIR[_cur_host()]):
        run('go get -u {}'.format(source))

def get_remote_db(working="stored_hashes.{}.sqlite3"):
    "download remote sqlite db from virdam"
    working = working.format(_cur_host())
    gzipped = "{}.{}.down.gz".format(working, NOW_FMT)
    localdir = os.path.join(LOCAL, 'db')
    print(env.hosts, _cur_host(), WORKDIR[_cur_host()], env.host)
    with cd(WORKDIR[_cur_host()] + '/db'):
        run('gzip -N -k -S .{}.down.gz {}'.format(NOW_FMT, working))
        get(gzipped, localdir)
    with lcd(localdir):
        local('gzip -d -c {} > {}'.format(gzipped, working))
