# ExPlore Go
Exploration and Proof-of-Concepts for Golang

## Ideas

* a web frontend for monitoring
* a notification mechanism (via email)
* a sha256 value calculator for numerous files (concurrency!)
* a catalogue reader/writer (sqlite?)

## First One

1. Read a directory, calculate all sha256 checksums
2. Read from a config file (checksum algorithm...)
3. Use "flag" for CLI options like directory
4. Write catalog entries to database

*OK* (no flags)

## Secondly

1. Which mechanism for comparison of sha256 values? Or pipelining 
   messages around (ZeroMQ or RabbitMQ or Consul or Raft)?
2. Which mechanism for notification in case of errors / inconsistencies etc.?
3. The presentation layer: A web server (cf. https://docs.gitea.io)


## Third Stroke

Walk a root directory and create hardlinked sha256 filenames, as well as
symlinked filenames, in a parallel root `sha256` directory, using a 
directory structure of 0-3/4-7 chars:

  data/2014/01/03/filename.ext
  sha256/7a70/5979/7a705979c0a0163d6289e6d1423ddcc8c7fad8dbc0773a9ae20a6bbcd8120dce  
  sha256/7a70/5979/7a705979c0a0163d6289e6d1423ddcc8c7fad8dbc0773a9ae20a6bbcd8120dce.lnk -> 
  	../2014/01/03/filename.ext

(use "flags" for this one!)

**OK**, works

The gist of this is its easy searchability and testability

Do not forget to log, and log os errors as well!

Logging seems to be somewhat tricky if you want to get it right.


### First estimation

ingesting 2.000 assets took 47 secs. (first version, writing into sqlite)

* 10.000 = 250 secs
* 100.000 = 2500 secs = ca 42 Min.
* 500.000 (e.g UNIDAM all assets 2018-04) = 7,5 hrs

**BUT** we might have to batch the results or else we keep all results in memory
until writing to sqlite




## For Later Usage

Lookitthat: https://github.com/avelino/awesome-go

* https://github.com/schollz/progressbar
* concurrently walk the file system: https://github.com/dixonwille/skywalker
* https://github.com/mattn/go-sqlite3 or the ORM: http://gorm.io/docs/
* Config processing: https://github.com/olebedev/config
* Logging: https://github.com/sirupsen/logrus
