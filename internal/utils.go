/*

Utilities for the explore project
*/
package utils

import (
	"os"
	"fmt"
	_ "time"
	_ "flag"
	"crypto/sha256"
	"crypto/md5"
	"log"
	"io"
	"strings"
	// "strconv"
	_ "os/exec"
	_ "bytes"
	"path"
	_"runtime"
	_ "path/filepath"
	"github.com/olebedev/config"
	 "github.com/jinzhu/gorm"
  	_ "github.com/jinzhu/gorm/dialects/sqlite"
  	"sync"
  	_ "sort"
    "github.com/dixonwille/skywalker"
   )



type ShaInfo struct {
	gorm.Model
	Filepath string
	Sha256 string
	Size int64
	StorageUnitID int32
} 

type StorageUnit struct {
	gorm.Model
	Name string
	Main_ip string
	Replicas string
}


type ShaWorker struct {
    *sync.Mutex
    found []ShaInfo
}

type ShaMD5Worker struct {
	*sync.Mutex
	found []string
}

func (shaw *ShaWorker) Work(path string) {
    //This is where the necessary work should be done.
    //This will get concurrently so make sure it is thread safe if you need info across threads.
    shaw.Lock()
    defer shaw.Unlock()
    sha256 := FileSha256(path)
    var size int64 = 0
    fileinfo, err := os.Stat(path)
	if err != nil {
		log.Fatal(err)
	} else {
		size = fileinfo.Size()
	}    
    shaw.found = append(shaw.found, 
    				ShaInfo{Filepath: path, Sha256: sha256, Size: size} )
}


// ShaMD5Writer writes a named hardlink into respective directory
func(shaw *ShaMD5Worker) Work(srcpath string) {
	shaw.Lock()
	defer shaw.Unlock()
	var hashes [2]string
	hashes = FileShaMD5(srcpath)
    for i, d := range dirnames {
        hashpath := path.Join(root, "..", d, hashes[i][:2], hashes[i][2:4]) 
        err := os.MkdirAll(hashpath, os.FileMode(0777))
        if err != nil {
            log.Fatal(err)
        }
        linktarget := path.Join(hashpath, hashes[i])
        err = os.Link(srcpath, linktarget)
        if err != nil {
            log.Fatal(err)
        }
        err = os.Symlink(srcpath, linktarget + ".lnk")
        if err != nil {
            log.Fatal(err)
        }
    }
}

// FileShaMD5 returns the sha256 hash for a given filepath
// to be called by a goroutine
func FileShaMD5(filepath string) [2]string {
	f, err := os.Open(filepath)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	s := sha256.New()
	m := md5.New()
	if _, err := io.Copy(s, f); err != nil {
		log.Fatal(err)
	}
	if _, err := io.Copy(m, f); err != nil {
		log.Fatal(err)
	}
	// fmt.Println(h.Sum(nil))
	return [2]string{fmt.Sprintf("%x", s.Sum(nil)), 
		fmt.Sprintf("%x", m.Sum(nil))}
}

// FileSha256 returns the sha256 hash for a given filepath
// to be called by a goroutine
func FileSha256(filepath string) string {
	f, err := os.Open(filepath)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	h := sha256.New()
	if _, err := io.Copy(h, f); err != nil {
		log.Fatal(err)
	}
	// fmt.Println(h.Sum(nil))
	return string(fmt.Sprintf("%x", h.Sum(nil)))
}

// GetHostname returns the local hostname (used for diffentiation of 
// yml-structure) by calling the hostname program on the local host
/* func GetHostname() string {
	cmd := exec.Command("/bin/hostname", "-f")
	var out bytes.Buffer
	cmd.Stdout = &out
	err := cmd.Run()
	if err != nil {
	    log.Fatal(err)
	}
	fqdn := out.String()
	fqdn = fqdn[:len(fqdn)-1]
	return fqdn
}
*/

// GetHostConfig returns the config section for the current host
func GetHostConfig() (*config.Config, string)  {
	hostname, err := os.Hostname()
	if err != nil {
		log.Fatal(err)
	}
	hostname = strings.Split(hostname, ".")[0]
    // _, filename, _, _ := runtime.Caller(1)
	filepath :=  "explore.yml"
	cfg, err := config.ParseYamlFile(filepath)
	if err != nil {
    	log.Fatal(err)
    }
    return cfg, hostname
}

func ProcessAssets() uint {
	cfg, hostname := GetHostConfig()
    dbname, err := cfg.String("common.dbname")
    if err != nil {
		log.Fatal(err)
	}
	dbdir, err := cfg.String(hostname + ".dbdir")
	if err != nil {
		log.Fatal(err)
	}
	testdir, err := cfg.String(hostname + ".testdir")
	if err != nil {
		log.Fatal(err)
	}
	db, err := gorm.Open("sqlite3", path.Join(dbdir, 
		dbname +  hostname + ".sqlite3"))

	if err != nil {
    	panic("failed to connect database")
  	}
  	defer db.Close()
	shaw := new(ShaWorker)
    shaw.Mutex = new(sync.Mutex)
    fmt.Println("starting in: ", testdir)

	// db.CreateTable(&ShaInfo{})
	// Migrate the schema
  	db.AutoMigrate(&ShaInfo{})
  	db.AutoMigrate(&StorageUnit{})
  	// root is the root directory of the data that was stood up above
    sw := skywalker.New(testdir, shaw)
    if hostname == "phoebe" {
    	sw.DirListType = skywalker.LTWhitelist
    	sw.DirList = []string{"/2018-01-20", "/2018-02-08"}
	}
    sw.ExtListType = skywalker.LTWhitelist
    extList := make([]string, 20)
    extListCfg, err := cfg.List(hostname + ".extensions") 
    if err != nil {
    	log.Fatal(err)
    }
    for i, item := range extListCfg {
    	extList[i] = item.(string)
    }
    sw.ExtList = extList
    walk_err := sw.Walk()
    if walk_err != nil {
        fmt.Println(walk_err)
        return 0
    }
    // sort.Sort(sort.StringSlice(shaw.found))
    log.Println(len(shaw.found), "assets found, updating db...")
    for _, f := range shaw.found {
        // fmt.Println(f[0]) // strings.Replace(f, sw.Root, "", 1))

        // var si = ShaInfo{Filepath: f.Filepath, Sha256: f.Sha256, Size: f.Size}
        db.Create(&f)
    }
    return uint(len(shaw.found))
}
