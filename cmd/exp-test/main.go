/*
Exploring and testing things

*/
package main

import (
	"fmt"
	"runtime"
	_ "os"
	"log"
	_ "github.com/olebedev/config"
	 _ "github.com/dixonwille/skywalker"
	 _ "sync"
     explore "ghe.phaidra.org/bekesij9/explore"
)





func main() {
	cfg, hostname := explore.GetHostConfig()

    // var myList = make([]string, 10)
    myList, err := cfg.List(hostname + ".extensions") 
    if err != nil {
    	log.Fatal(err)
    }
    // we have to init a slice
    braz := make([]string, 10)
    // and then loop over the interface 
    for i, item := range myList {
    	fmt.Println(item)
	    // using type assertion:
    	braz[i] = item.(string)
    }
    fmt.Println("new slice from yml: %v", braz)
    //sw := skywalker.New("./", shaw)
    // sw.ExtListType = skywalker.LTWhitelist
    // sw.ExtList = braz
	fmt.Println("HOST        :", hostname)
	fmt.Println("  GOMAXPROCS:", runtime.GOMAXPROCS(-1))
	fmt.Println("  NumCPU    :", runtime.NumCPU())
}