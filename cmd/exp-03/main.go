/*
Exploring and testing things

*/
package main

import (
	"fmt"
	_"runtime"
	"os"
	"log"
    "time"
	// _ "github.com/olebedev/config"
	//  _ "github.com/dixonwille/skywalker"
	// _ "sync"
    "flag"
    "path"
    "path/filepath"
    "ghe.phaidra.org/bekesij9/bispic"
    
)

var (
   root string
   usage = "Usage: ./exp03 <rootdirectory>\n" 
   dirnames = []string{"sha256"}

)

func init() {
    flag.StringVar(&root, "root", "./test", "Root directory to begin processing with")
}

// func os.Link(oldname, newname string) error
// func Symlink(oldname, newname string) error


func main() {
    flag.Parse()
    root, err := filepath.Abs(root)
    if err != nil {
        log.Fatal(err)
    }
    fmt.Println("root:", root)
    bispic.Root = root
    bispic.Dirnames = dirnames
    t0 := time.Now()
    total := bispic.ProcessAssetsWriteFS()
    t1 := time.Now()
    log.Printf("...ingesting %v assets took %v to run.\n", total, t1.Sub(t0))
}



func mainOld() {
	flag.Parse()
    root, err := filepath.Abs(root)
    if err != nil {
        log.Fatal(err)
    }
    fmt.Println("root:", root)
    // InitDirs(root)
    var hashes [2]string
    srcpath := root + "/montafon2017/P1050830.JPG"
    bispic.Root = root
    bispic.Dirnames = dirnames
    hashes = bispic.FileShaMD5(srcpath)
    for i, d := range bispic.Dirnames {
        pt1 := hashes[i][:2]
        pt2 := hashes[i][2:4]
        // fmt.Println("path:", srcpath, d, pt1, pt2, "--", hashes[i])
        hashpath := path.Join(bispic.Root, "..", d, pt1, pt2) 
        err := os.MkdirAll(hashpath, os.FileMode(0777))
        if err != nil {
            log.Fatal(err)
        }
        linktarget := path.Join(hashpath, hashes[i])
        err = os.Link(srcpath, linktarget)
        if err != nil {
            log.Fatal(err)
        }
        err = os.Symlink(srcpath, linktarget + ".lnk")
        if err != nil {
            log.Fatal(err)
        }

    }
	/* fmt.Println("HOST        :", hostname)
	fmt.Println("  GOMAXPROCS:", runtime.GOMAXPROCS(-1))
	fmt.Println("  NumCPU    :", runtime.NumCPU())
    */
}

// InitDirs creates md5 and sha256 directories
func InitDirs(root string) {
    for _, d := range dirnames {
        fmt.Println(d)
        err := os.MkdirAll(path.Join(root, "..", d), os.FileMode(0777))
        if err != nil {
            log.Fatal(err)
        }
    }
}