/*
Read a directory, calculate all sha256 checksums of files of a type

(add package with "go get github.com/olebedev/config")

Try "skywalker" package for goroutine directory walking

Try GORM for storing results in sqlite (cf. https://gorm.io, please do
	implicit `go get github.com/mattn/go-sqlite3`)


*/
package main

import (
	_ "os"
	"fmt"
	"time"
	_ "crypto/sha256"
	"log"
	_ "io"
	_ "strings"
    "ghe.phaidra.org/bekesij9/explore"    
   )



func main() {
	cfg, hostname := explore.GetHostConfig()
    dbname, err := cfg.String("common.dbname")
    if err != nil {
		log.Fatal(err)
	}
	dbdir, err := cfg.String(hostname + ".dbdir")
	if err != nil {
		log.Fatal(err)
	}
	testdir, err := cfg.String(hostname + ".testdir")
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("testdir:", testdir, "dbname:", dbdir + "/" + dbname)
	t0 := time.Now()

	total := explore.ProcessAssets()
	t1 := time.Now()
	log.Printf("...ingesting %v assets took %v to run.\n", total, t1.Sub(t0))	

}


